# Core Bin System Files

This repository holds the source code for the core system files, drivers, provisioning files, etc. 
for the InventOne Board, allowing Over-The-Air flashing of the microcontroller.

We are unlikely to accept suggestions about how to grow this project into something it could be.
Please keep that in mind before posting issues and PRs if this Project is ever made public.

## Prerequisites
* None
## Updates
* Important note Updates to the core system files will be critically reviewed
* Please do not try to push to ***master*** branch, it won't work :)

```This repository is maintained by InventOne Team```
