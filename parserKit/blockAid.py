import json  # Required for parsing JSON payload
import re
from .pinAid import getBoardPin
from .codeAid import blockToBody, blockToInclude, blockToMake, blockToInit


class Actuator(object):

    """
    Support is available for the following actuators:
    > L298N
    > ULN2003
    """

    def __init__(self, dTopic, id, pins):
        self.dTopic = dTopic
        self.id = id
        self.pins = pins
        for pin in pins.keys():
            self.pins[pin] = getBoardPin(pins[pin])
        self.args = ''
        self.codeBlock = []
        self.includeBlock = []
        self.initBlock = []
        self.makeBlock = []
        self.setCodeBlock()
        self.setIncludeBlock()
        self.setInitBlock()
        self.setMakeBlock()
        self.getArguments()

    def setCodeBlock(self):
        if self.id == "L298N":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "ULN2003":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "led":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "relay":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "dc":
            self.codeBlock = blockToBody['analogWrite']
        else:
            self.codeBlock = blockToBody['led']

    def setIncludeBlock(self):
        if self.id == "L298N":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "ULN2003":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "dc":
            self.includeBlock = blockToInclude['analogWrite']
        else:
            self.includeBlock = blockToInclude['led']

    def setInitBlock(self):
        if self.id == "L298N":
            self.initBlock = blockToInit[self.id]
        elif self.id == "ULN2003":
            self.initBlock = blockToInit[self.id]
        elif self.id == "led":
            self.initBlock = blockToInit[self.id]
        elif self.id == "relay":
            self.initBlock = blockToInit[self.id]
        elif self.id == "dc":
            self.initBlock = blockToInit['analogWrite']
        else:
            self.initBlock = blockToInit['led']

    def setMakeBlock(self):
        if self.id == "L298N":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "ULN2003":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "dc":
            self.makeBlock = blockToMake['analogWrite']
        else:
            self.makeBlock = blockToMake['led']

    def addInput(self, input):
        self.pins['other'] = input

    def getArguments(self):
        pass


class Display(object):

    """
    Support is available for the following displays:
    > Dot Matrix Display (with MAX7219)
    > LCD
    """

    def __init__(self, tag, id, pins, config):
        self.tag = tag
        self.id = id
        self.pins = pins
        self.config = config
        for pin in pins.keys():
            self.pins[pin] = getBoardPin(pins[pin])
        self.args = ''
        self.codeBlock = []
        self.includeBlock = []
        self.initBlock = []
        self.makeBlock = []
        self.setCodeBlock()
        self.setIncludeBlock()
        self.setInitBlock()
        self.setMakeBlock()
        self.getArguments()

    def setCodeBlock(self):
        if self.id == "dotmatrix":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "lcd":
            self.codeBlock = blockToBody[self.id]

    def setIncludeBlock(self):
        if self.id == "dotmatrix":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "lcd":
            self.includeBlock = blockToInclude[self.id]

    def setInitBlock(self):
        if self.id == "dotmatrix":
            self.initBlock = blockToInit[self.id]
        elif self.id == "lcd":
            self.initBlock = blockToInit[self.id]

    def setMakeBlock(self):
        if self.id == "dotmatrix":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "lcd":
            self.makeBlock = blockToMake[self.id]

    def addInput(self, input):
        self.pins['other'] = input

    def getArguments(self):
        pass


class Function(object):

    """
    Support is available for the following functions:
    > Functions
        ** Analog Read
        ** Digital Read
        ** Digital Write
        ** Analog Write
        ** map
    """

    def __init__(self, tag, id, pins, unit, dTopic, dataClients, config):
        self.tag = tag
        self.id = id
        self.pins = pins
        for pin in pins.keys():
            self.pins[pin] = getBoardPin(pins[pin])
        self.args = ''
        self.unit = unit
        self.config = config
        self.dTopic = dTopic
        self.dataClients = dataClients
        self.codeBlock = []
        self.includeBlock = []
        self.initBlock = []
        self.makeBlock = []
        self.setCodeBlock()
        self.setIncludeBlock()
        self.setInitBlock()
        self.setMakeBlock()
        #self.args = [ getBoardPin(pin) for pin in self.pins.values() ][0]
        self.getArguments()

    def setCodeBlock(self):
        if self.id == "read_data":
            if "adc" in "".join(self.pins):
                self.codeBlock = blockToBody["analogRead"]
            else:
                self.codeBlock = blockToBody["digitalRead"]

        elif self.id == "write_data":
            if "adc" in "".join(self.pins):
                self.codeBlock = blockToBody["analogWrite"]
            else:
                self.codeBlock = blockToBody["digitalWrite"]

    def setIncludeBlock(self):
        if self.id == "read_data":
            if "adc" in "".join(self.pins):
                self.includeBlock = blockToInclude["analogRead"]
            else:
                self.includeBlock = blockToInclude["digitalRead"]

        elif self.id == "write_data":
            if "adc" in "".join(self.pins):
                self.includeBlock = blockToInclude["analogWrite"]
            else:
                self.includeBlock = blockToInclude["digitalWrite"]

    def setInitBlock(self):
        if self.id == "read_data":
            if "adc" in "".join(self.pins):
                self.initBlock = blockToInit["analogRead"]
            else:
                self.initBlock = blockToInit["digitalRead"]

        elif self.id == "write_data":
            if "adc" in "".join(self.pins):
                self.initBlock = blockToInit["analogWrite"]
            else:
                self.initBlock = blockToInit["digitalWrite"]

    def setMakeBlock(self):
        if self.id == "read_data":
            if "adc" in "".join(self.pins):
                self.makeBlock = blockToMake["analogRead"]
            else:
                self.makeBlock = blockToMake["digitalRead"]

        elif self.id == "write_data":
            if "adc" in "".join(self.pins):
                self.makeBlock = blockToMake["analogWrite"]
            else:
                self.makeBlock = blockToMake["digitalWrite"]

    def getArguments(self):
        pass


class Module(object):

    """
    Support is available for the following modules:
    > RTC module (ds1302, ds1307)
    """

    def __init__(self, tag, id, pins, dTopic, dataClients):
        self.tag = tag
        self.id = id
        self.pins = pins
        for pin in pins.keys():
            self.pins[pin] = getBoardPin(pins[pin])
        self.args = ''
        self.dTopic = dTopic
        self.dataClients = dataClients
        self.codeBlock = []
        self.includeBlock = []
        self.initBlock = []
        self.makeBlock = []
        self.setCodeBlock()
        self.setIncludeBlock()
        self.setInitBlock()
        self.setMakeBlock()
        self.getArguments()

    def setCodeBlock(self):
        if self.id == "ds1302":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "ds1307":
            self.codeBlock = blockToBody[self.id]

    def setIncludeBlock(self):
        if self.id == "ds1302":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "ds1307":
            self.includeBlock = blockToInclude[self.id]

    def setInitBlock(self):
        if self.id == "ds1302":
            self.initBlock = blockToInit[self.id]
        elif self.id == "ds1307":
            self.initBlock = blockToInit[self.id]

    def setMakeBlock(self):
        if self.id == "ds1302":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "ds1307":
            self.makeBlock = blockToMake[self.id]

    def getArguments(self):
        pass


class Sensor(object):

    """
    Support available for the following sensors:
    > Temperature Sensor
        *DS18B20
        *LM35
        *DHT11
        *TMP35
        *TMP36
        *TMP37
    > Gas Sensor
    > Pir Sensor
    > Microphone Sensor
    > Light Sensor
        *TSL2561
        *LDR
    > Ultrasonic Sensor
        *HC-SR04
    > Button
    """

    def __init__(self, tag, id, pins, unit, dTopic, tTopic, threshold, dataClients, thresholdClients):
        self.tag = tag
        self.id = id
        self.pins = pins
        for pin in pins.keys():
            self.pins[pin] = getBoardPin(pins[pin])
        self.args = ''
        self.unit = unit
        self.dTopic = dTopic
        self.tTopic = tTopic
        self.threshold = threshold
        self.dataClients = dataClients
        self.thresholdClients = thresholdClients
        self.codeBlock = []
        self.includeBlock = []
        self.initBlock = []
        self.makeBlock = []
        self.setCodeBlock()
        self.setIncludeBlock()
        self.setInitBlock()
        self.setMakeBlock()
        self.getArguments()

    def setCodeBlock(self):
        if self.id == "ds18b20":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "lm35":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "dht21":
            suffix = "T"
            if self.unit.startswith('p'):
                suffix = "H"
            self.codeBlock = blockToBody[self.id+suffix]
        elif self.id == "tmp35":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "tmp36":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "tmp37":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "mq2":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "pir":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "mic":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "tsl2561":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "ldr":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "hc-sr04":
            self.codeBlock = blockToBody[self.id]
        elif self.id == "button":
            self.codeBlock = blockToBody[self.id]
        else:
            if 'adc' in ''.join(self.pins):
                self.codeBlock = blockToBody['analogRead']
            else:
                self.codeBlock = blockToBody['digitalRead']

    def setIncludeBlock(self):
        if self.id == "ds18b20":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "lm35":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "dht21":
            suffix = "T"
            if self.unit.startswith('p'):
                suffix = "H"
            self.includeBlock = blockToInclude[self.id+suffix]
        elif self.id == "tmp35":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "tmp36":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "tmp37":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "mq2":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "pir":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "mic":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "tsl2561":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "ldr":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "hc-sr04":
            self.includeBlock = blockToInclude[self.id]
        elif self.id == "button":
            self.includeBlock = blockToInclude[self.id]
        else:
            if 'adc' in ''.join(self.pins):
                self.includeBlock = blockToInclude['analogRead']
            else:
                self.includeBlock = blockToInclude['digitalRead']
    
    def setInitBlock(self):
        if self.id == "ds18b20":
            self.initBlock = blockToInit[self.id]
        elif self.id == "lm35":
            self.initBlock = blockToInit[self.id]
        elif self.id == "dht21":
            suffix = "T"
            if self.unit.startswith('p'):
                suffix = "H"
            self.initBlock = blockToInit[self.id+suffix]
        elif self.id == "tmp35":
            self.initBlock = blockToInit[self.id]
        elif self.id == "tmp36":
            self.initBlock = blockToInit[self.id]
        elif self.id == "tmp37":
            self.initBlock = blockToInit[self.id]
        elif self.id == "mq2":
            self.initBlock = blockToInit[self.id]
        elif self.id == "pir":
            self.initBlock = blockToInit[self.id]
        elif self.id == "mic":
            self.initBlock = blockToInit[self.id]
        elif self.id == "tsl2561":
            self.initBlock = blockToInit[self.id]
        elif self.id == "ldr":
            self.initBlock = blockToInit[self.id]
        elif self.id == "hc-sr04":
            self.initBlock = blockToInit[self.id]
        elif self.id == "button":
            self.initBlock = blockToInit[self.id]
        else:
            if 'adc' in ''.join(self.pins):
                self.initBlock = blockToInit['analogRead']
            else:
                self.initBlock = blockToInit['digitalRead']


    def setMakeBlock(self):
        if self.id == "ds18b20":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "lm35":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "dht21":
            suffix = "T"
            if self.unit.startswith('p'):
                suffix = "H"
            self.makeBlock = blockToMake[self.id+suffix]
        elif self.id == "tmp35":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "tmp36":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "tmp37":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "mq2":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "pir":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "mic":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "tsl2561":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "ldr":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "hc-sr04":
            self.makeBlock = blockToMake[self.id]
        elif self.id == "button":
            self.makeBlock = blockToMake[self.id]
        else:
            if 'adc' in ''.join(self.pins):
                self.makeBlock = blockToMake['analogRead']
            else:
                self.makeBlock = blockToMake['digitalRead']

    def getArguments(self):
        pass
        

class Utility(object):

    """
    Support available for the following utilities:
    > Mux
    > Pass
    > Threshold
    > Math (+,-,*,sin,cos,/,tan)
    > Constant
    > Map
    """

    def __init__(self, name, tag, config, dTopic, dataClients):
        self.name = name
        self.tag = tag
        self.config = config
        self.dTopic = dTopic
        self.dataClients = dataClients
        self.codeBlock = []
        self.includeBlock = []
        self.makeBlock = []
        self.initBlock = []
        self.setCodeBlock()
        self.setIncludeBlock()
        self.setInitBlock()
        self.setMakeBlock()

    def setCodeBlock(self):
        if self.name == "map":
            self.codeBlock = blockToBody[self.name]
        elif self.name == "mux":
            self.codeBlock = ['float {refVar} = '+self.config['c_value'].format(**tempDict)+';']
        elif self.name == "passer":
            self.codeBlock = ['if (%s) {{\n\t\t{refVar} = %s;\n\t}}'%([i.strip() for i in str(self.config['c_value']).split('?')][0], [i.strip() for i in str(self.config['c_value']).split('?')][1])]
        elif self.name == "constant":
            self.codeBlock = ['float {refVar} = '+ str(self.config['c_value'])+';']
        elif self.name == "add":
            self.codeBlock = ['float {refVar} = '+ str(self.config['c_value'])+';']
        elif self.name == "substract":
            self.codeBlock = ['float {refVar} = '+ str(self.config['c_value'])+';']
        elif self.name == "multiply":
            self.codeBlock = ['float {refVar} = '+ str(self.config['c_value'])+';']
        elif self.name == "divide":
            self.codeBlock = ['float {refVar} = '+ str(self.config['c_value'])+';']
        elif self.name == "sin":
            self.codeBlock = ['float {refVar} = '+ self.config['c_value'].format(dict(sin='sinf'))+';']
        elif self.name == "cos":
            self.codeBlock = ['float {refVar} = '+ self.config['c_value'].format(dict(sin='cosf'))+';']
        elif self.name == "tan":
            self.codeBlock = ['float {refVar} = '+self.config['c_value'].format(dict(sin='tanf'))+';']
        else:
            self.codeBlock = ['float {refVar} = '+ str(self.config['c_value'])+';']
    
    def setIncludeBlock(self):
        if self.name == "map":
            self.includeBlock = blockToInclude[self.name]
    
    def setInitBlock(self):
        if self.name == "passer":
            self.initBlock = blockToInit[self.name]
    
    def setMakeBlock(self):
        if self.name == "map":
            self.makeBlock = blockToMake[self.name]


class Widget(object):

    def __init__(self, name, tag):
        self.name = name
        self.tag = tag


def getActuators(jsonData):
    lis = []
    for block in jsonData:
        if block["b_type"] == "actuator":
            lis.append(
                Actuator(
                    block["tag"], block["setting"]["type"], block["setting"]["pins"]
                )
            )
    return lis


def getDisplays(jsonData):
    lis = []
    for block in jsonData:
        if block["b_type"] == "display":
            lis.append(
                Display(
                    block["tag"], block["b_class"],
                    block["setting"]["pins"],
                    block["setting"]["config"]
                )
            )
    return lis


def getFunctions(jsonData):
    lis = []
    for block in jsonData:
        if block["b_type"] == "function" or '_data' in block['b_class']:
            if 'read' in block['b_class']:
                temp = block["setting"]["data"]["d_actions"]
                other = block["setting"]["data"]["d_topic"]
            else:
                temp = []
                other = ''
            lis.append(
                Function(
                    block["tag"],
                    block["b_class"],
                    block["setting"]["pins"],
                    block["setting"]["unit"],
                    other,
                    temp,
                    block["setting"]["config"]
                )
            )
    return lis


def getModules(jsonData):
    lis = []
    for block in jsonData:
        if block["b_type"] == "module":
            lis.append(
                Module(
                    block["tag"],
                    block["setting"]["type"],
                    block["setting"]["pins"],
                    block['setting']['data']['d_topic'],
                    block["setting"]["data"]["d_actions"],
                )
            )
    return lis


def getSensors(jsonData):
    lis = []
    for block in jsonData:
        if block["b_type"] == "sensor":
            if len(block['setting']['threshold']) > 2:
                lis.append(
                    Sensor(
                        block["tag"],
                        block["setting"]["type"],
                        block["setting"]["pins"],
                        block["setting"]["unit"],
                        block['setting']['data']['d_topic'],
                        block['setting']['threshold']['t_topic'],
                        block["setting"]["threshold"]["t_value"],
                        block["setting"]["data"]["d_actions"],
                        block["setting"]["threshold"]["t_actions"],
                    )
                )
            else:
                s = block["setting"]["data"]["d_actions"]
                lis.append(
                    Sensor(
                        block["tag"],
                        block["setting"]["type"],
                        block["setting"]["pins"],
                        block["setting"]["unit"],
                        block['setting']['data']['d_topic'],
                        block['setting']['threshold']['t_topic'],
                        block["setting"]["threshold"]["t_value"],
                        s,
                        [],
                    )
                )
    return lis


def getUtilities(jsonData):
    lis = []
    for block in jsonData:
        if block["b_type"] == "utility" and '_data' not in block['b_class']:
            lis.append(
                Utility(
                    block["name"],
                    block["tag"],
                    block["setting"]["config"],
                    block['setting']['data']['d_topic'],
                    block["setting"]["data"]["d_actions"],
                )
            )
    return lis


def getWidgets(jsonData):
    lis = []
    for block in jsonData:
        if block["b_type"] == "widget":
            lis.append(Widget(block["name"], block["tag"]))
    return lis