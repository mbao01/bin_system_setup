import os  # Required for creating, renaming, and moving files
import shutil  # Required for copying files and deleting file trees
import subprocess  # Required for running terminal commands from script


# Builds Makefile and returns url of the iofirmware.bin file
def getBinaryUrl(boardsDir, boardId, host):
    try:
        shutil.rmtree(boardsDir+boardId+'/build')
        shutil.rmtree(boardsDir+boardId+'/firmware')
    except:
        pass

    subprocess.call("make", cwd=boardsDir+boardId, stdout=subprocess.DEVNULL)
    if os.path.exists(boardsDir + os.sep + boardId+ os.sep + 'firmware'):
        return '{host}/{boardId}/firmware/iofirmware.bin'.format(host=host, boardId=boardId)
    return ''


# Inserts into C template and stores it in a boardId named subfolder
def insertCBlock(includeBlock, initBlock, bodyBlock, boardsDir, htmlDir, cTemplate, boardId):
    with open(cTemplate, 'r') as myFile:
        data = myFile.read().replace('<bodyBlock>', bodyBlock).replace(
            '<includeBlock>', includeBlock).replace('<initBlock>', initBlock)

    if not os.path.exists(boardsDir + os.sep + boardId):
        os.makedirs(boardsDir + os.sep + boardId)
        shutil.copytree(htmlDir, boardsDir+os.sep+boardId+os.sep+'html')

    with open(boardsDir + os.sep + boardId + os.sep + 'user_main.c', 'w') as myFile:
        print(data, file=myFile)


# Inserts into Makefile template and stores it in a boardId named subfolder
def insertMakeBlock(makeBlock, boardsDir, htmlDir, makefile, boardId):
    with open(makefile, 'r') as myfile:
        data = myfile.read().replace('<makeBlock>', makeBlock)

    if not os.path.exists(boardsDir + os.sep + boardId):
        os.makedirs(boardsDir + os.sep + boardId)
        shutil.copytree(htmlDir, boardsDir+os.sep+boardId+os.sep+'html')

    with open(boardsDir + os.sep + boardId + os.sep + 'Makefile', 'w') as myFile:
        print(data, file=myFile)
