blockToBody = dict(
    lcd = [
        """
        i2c_lcd_set_cursor(&lcd, 0, 0);
        char str[32] = {{}};
        snprintf(str, 32, "%s", {string});
        i2c_lcd_write(&lcd, strlen(str), str);
        """
    ],
    analogRead = [
        'float {refVar} = uint16_t analogRead({pins0});'
    ],
    analogWrite = [
        'analogWriteSingle({pins0}, {state});'
    ],
    digitalRead = [
        'float {refVar} = uint8_t digitalRead({pins0});'
    ],
    digitalWrite = [
        'digitalWrite({pins0}, {state});'
    ],
    map = [
        "float {refVar} = map({val}, {in_min}, {in_max}, {out_min}, {out_max});"
    ],
    led = [
        "digitalWrite({pins0}, {state});"
    ],
    relay = [
        "digitalWrite({pins0}, {state});"
    ],
    ds18b20 = [
        "float {refVar} = dS18b20_getTemp({pins0}, {unit});"
    ],
    hc-sr04 = [
        """
        int {refVar} = hcsr04_getDistance({pins0}, {pins1}, {unit});
        hcsr04_delay_ms(ms);
        """   
    ],
    ldr = [
        "float {refVar} = ldr_getLUX({pins0});"    
    ],
    dht21T = [
        "float {refVar} = dht11_getTemp({pins0}, {unit});"
    ],
    dht21H = [
        "float {refVar} = dht11_getHumidity({pins0}, {unit});"
    ],
    dotmatrix = [
        """
        max7219_dotMatrix_letter(&disp, {value} + '0');
        vTaskDelay(500/portTICK_PERIOD_MS);
        max7219_dotMatrix_letter(&disp, '0');
        vTaskDelay(500/portTICK_PERIOD_MS);
        """
    ],
    button = [
        """
        uint32_t button_ts;
        int {refVar} = (int)xQueueReceive(_button_h_tsqueue, &button_ts, 0);
        """
    ],
    pir = [
        """
        uint32_t pir_ts;
        int pir_val = (int)xQueueReceive(_pir_h_tsqueue, &pir_ts, 0);
        """
    ]
)

blockToInit = dict(
    lcd = [
        """
        i2c_lcd_t lcd_t = {{0, {pins1} ,{pins0}, 0x3f}};
        hd44780_t lcd = {{
        .i2c_dev.bus = lcd_t.i2c_bus,
        .i2c_dev.addr = lcd_t.addr,
        .font = HD44780_FONT_5X8,
        .lines = 2,
        .pins = {{.rs = 0, .e = 2, .d4 = 4, .d5 = 5, .d6 = 6, .d7 = 7, .bl = 3}},
        .backlight = true
        }};
        i2c_lcd_init(&lcd_t, &lcd);
        i2c_lcd_clear(&lcd);
        """
    ],
    pir = [
        """
        gpio_enable({pins0}, GPIO_INPUT); replace gpio with pin here
        _pir_h_tsqueue = xQueueCreate(2, sizeof(uint32_t));
        gpio_set_interrupt({pins0}, GPIO_INTTYPE_EDGE_POS, gpio_intr_handler);   
        """
    ],
    passer = [
        "float {refVar};"    
    ],
    analogRead = [],
    analogWrite = [],
    digitalRead = [],
    digitalWrite = [],
    map = [],
    ds18b20 = [],
    led = [],
    relay = [],
    dotmatrix = [
        """
        max7219_display_t disp = {{{pins2}, {digits}, {cascade_size}, true}};
        max7219_init(&disp);
        """
    ],
    button = [
        """
        gpio_enable({pins0}, GPIO_INPUT);
        _button_h_tsqueue = xQueueCreate(2, sizeof(uint32_t));
        gpio_set_interrupt(pin, GPIO_INTTYPE_EDGE_NEG, gpio_intr_handler);
        """
    ],
    hc-sr04 =[],
    ldr = [],
    dht21T = [],
    dht21H = []
)

blockToInclude = dict(
    lcd = [
        "#include <i2c/i2c.h>",
        "#include <hd44780/hd44780.h>",
        "#include <i2c_lcd/i2c_lcd.h>"
    ],
    led = [
        "#include <functions/functions.h>"
    ],
    hc-sr04 = [
        '#include "hcsr04/hcsr04.h"'    
    ],
    ldr = [
        '#include <ldr/ldr.h>'    
    ],
    dht21T = [
        '#include <dht11/dht11.h>'
    ],
    dht21T = [
        '#include <dht11/dht11.h>'
    ],
    analogRead = [
        "#include <functions/functions.h>"
    ],
    analogWrite = [
        "#include <functions/functions.h>"
    ],
    digitalRead = [
        "#include <functions/functions.h>"
    ],
    digitalWrite = [
        "#include <functions/functions.h>"
    ],
    map = [
        "#include <functions/functions.h>"
    ],
    ds18b20 = [
        '#include <dS18b20_temp/dS18b20_temp.h>'
    ],
    dotmatrix = [
        "#include <max7219_dotMatrix/max7219_dotMatrix.h>"
    ],
    button = [
        '#include <button/button.h>'
    ],
    pir = [
        '#include "pir/pir.h"'
    ]
)

blockToMake = dict(
    lcd = [
        "<extension>extras/i2c_lcd",
        "<extension>extras/i2c",
        "<extension>extras/pcf8574",
        "<extension>extras/hd44780"
    ],
    analogRead = [
        "<extension>extras/pwm",
        "<extension>extras/functions"
    ],
    analogWrite = [
        "<extension>extras/pwm",
        "<etension>extras/functions"
    ],
    digitalRead = [
        "<extension>extras/pwm",
        "<extension>extras/functions"
    ],
    digitalWrite = [
        "<extension>extras/pwm",
        "<extension>extras/functions"
    ],
    map = [
        "<extension>extras/pwm",
        "<extension>extras/functions"
    ],
    ds18b20 = [
        '<extension>extras/dS18b20_temp',
        '<extension>extras/onewire',
        '<extension>extras/ds18b20'
    ],
    dotmatrix = [
        "<extension>extras/max7219_dotMatrix"
    ],
    led = [
        "<extension>extras/pwm",
        "<extension>extras/functions"
    ],
    hc-sr04 = [
        """
        <extension>extras/ultrasonic
        <extension>extras/hcsr04
        """
    ],
    ldr = [
        '<extension>extras/ldr'    
    ],
    dht21T = [
        """
        <extension>extras/dht11 
        <extension>extras/dht
        """
    ],
    dht21T = [
        """
        <extension>extras/dht11 
        <extension>extras/dht
        """
    ],
    button = [
        '<extension>extras/button'
    ],
    pir = [
        '<extension>extras/pir'
    ]
)
