pinMap = {
    '1': '0', '2': '1', '3': '2', '4': '3',
    '5': '4', '6': '5', '7': '12', '8': '13',
    '9': '14', '10': '15', '11': '16'
}

def getBoardPin(value):  # Required for translating from laboratory suite to board pin representation
    val = ''.join([element for element in value if element.isdigit()])
    if 'adc' not in value:
        if not val or val == '0':
            return 0
        return pinMap[val]
    return val
