class Suite(object):
    """
    Required for smart handling of blocks to generate
    corresponding C code and makefile content
    """
    def __init__(
        self, actuators, displays, functions, modules,
        sensors, utilities, widgets
        ):
        self.actuators = actuators
        self.displays = displays
        self.functions = functions
        self.modules = modules
        self.sensors = sensors
        self.utilities = utilities
        self.widgets = widgets
        self.tag2Ref = dict()
        self.ref2Lab = dict()
        self.refInput = dict()
        self.bodyBlock = []
        self.initBlock = []
        self.includeBlock = set()
        self.makeBlock = set()

    def connectBlocks(self):
        varSuffix = 0
        threshSuffix = 0

        for sensor in self.sensors:
            refVar = 'var%d' % varSuffix
            refThresh = 'thresh%d' % threshSuffix
            varSuffix += 1
            threshSuffix += 1

            self.tag2Ref[sensor.dTopic] = refVar
            self.tag2Ref[sensor.tTopic] = refThresh
            self.initBlock.append(
                '\n'.join(sensor.initBlock)
                .format(
                        refVar = refVar,
                        refThresh = refThresh,
                        **sensor.pins
                    )
                )
            self.bodyBlock.extend(
                [
                    '\n'.join(sensor.codeBlock)
                    .format(
                            refVar = refVar,
                            refThresh = refThresh,
                            unit = sensor.unit.upper(),
                            **sensor.pins
                        ),
                    'bool {refThresh} = {refVar} > {thresh};'
                    .format(
                            refVar = refVar,
                            refThresh = refThresh,
                            thresh = sensor.threshold or "0"
                        )
                    ]
                )
            self.includeBlock = self.includeBlock.union(set(sensor.includeBlock))
            self.makeBlock = self.makeBlock.union(set(sensor.makeBlock))

            for tag in sensor.dataClients:
                if tag.startswith('av') or tag.startswith('ld') or tag.startswith('co'):
                    self.refInput[tag[:-3]] = refVar
                else:
                    self.refInput[tag] = refVar
            for tag in sensor.thresholdClients:
                if tag.startswith('av') or tag.startswith('ld') or tag.startswith('co'):
                    self.refInput[tag[:-3]] = refThresh
                else:
                    self.refInput[tag] = refThresh

        for function in self.functions:
            refVar = 'var%d' % varSuffix
            varSuffix += 1

            self.tag2Ref[function.dTopic] = refVar
            if not function.config: function.config = dict()
            function.config.update(function.pins)
            self.initBlock.append(
                '\n'.join(function.initBlock)
                .format(
                    refVar = refVar,
                    state = '%s ? 1 : 0' % self.refInput.get(function.tag, 1),
                    input = self.tag2Ref.get(function.config.get('c_value', '  ')[1:-1], ''),
                    **function.config
                    )
                )
            if not function.config: function.config = dict()
            self.bodyBlock.append(
                '\n'.join(function.codeBlock)
                .format(
                    refVar = refVar,
                    state = '%s ? 1 : 0' % self.refInput.get(function.tag, 1),
                    **function.config
                    )
                )

            self.includeBlock = self.includeBlock.union(set(function.includeBlock))
            self.makeBlock = self.makeBlock.union(set(function.makeBlock))

            for tag in function.dataClients:
                if tag.startswith('av') or tag.startswith('ld') or tag.startswith('co'):
                    self.refInput[tag[:-3]] = refVar
                else:
                    self.refInput[tag] = refVar

        for module in self.modules:
            refVar = 'var%d' % varSuffix
            varSuffix += 1

            self.tag2Ref[module.dTopic] = refVar

            self.initBlock.append(
                '\n'.join(module.initBlock)
                .format(
                    refVar = refVar,
                    **module.pins
                    )
                )

            self.bodyBlock.append(
                '\n'.join(module.codeBlock)
                .format(
                    refVar = refVar,
                    **module.pins
                    )
                )

            self.includeBlock = self.includeBlock.union(set(module.includeBlock))
            self.makeBlock = self.makeBlock.union(set(module.makeBlock))

            for tag in module.dataClients:
                if tag.startswith('av') or tag.startswith('ld') or tag.startswith('co'):
                    self.refInput[tag[:-3]] = refVar
                else:
                    self.refInput[tag] = refVar

        for utility in self.utilities:
            if utility.name == 'constant':
                refVar = 'var%d' % varSuffix
                varSuffix += 1
    
                self.tag2Ref[utility.dTopic] = refVar
                
                self.initBlock.append(
                    '\n'.join(utility.initBlock)
                    .format(
                        refVar = refVar,
                        **self.tag2Ref
                        )
                    )
                utility.config.update(self.tag2Ref)
                self.bodyBlock.append(
                    '\n'.join(utility.codeBlock)
                    .format(
                        refVar = refVar,
                        val = self.refInput.get(utility.tag, ''),
                        **utility.config
                        )
                    )
    
                self.includeBlock = self.includeBlock.union(set(utility.includeBlock))
                self.makeBlock = self.makeBlock.union(set(utility.makeBlock))
    
                for tag in utility.dataClients:
                    if tag.startswith('av') or tag.startswith('ld') or tag.startswith('co'):
                        self.refInput[tag[:-3]] = refVar
                    else:
                        self.refInput[tag] = refVar

        for utility in self.utilities:
            if utility.name != 'constant':
                refVar = 'var%d' % varSuffix
                varSuffix += 1
    
                self.tag2Ref[utility.dTopic] = refVar
                
                self.initBlock.append(
                    '\n'.join(utility.initBlock)
                    .format(
                        refVar = refVar,
                        **self.tag2Ref
                        )
                    )
                utility.config.update(self.tag2Ref)
                self.bodyBlock.append(
                    '\n'.join(utility.codeBlock)
                    .format(
                        refVar = refVar,
                        val = self.refInput.get(utility.tag, ''),
                        **utility.config
                        )
                    )
    
                self.includeBlock = self.includeBlock.union(set(utility.includeBlock))
                self.makeBlock = self.makeBlock.union(set(utility.makeBlock))
    
                for tag in utility.dataClients:
                    if tag.startswith('av') or tag.startswith('ld') or tag.startswith('co'):
                        self.refInput[tag[:-3]] = refVar
                    else:
                        self.refInput[tag] = refVar


        for display in self.displays:
            display.addInput(self.refInput[display.tag])
            display.config.update(display.pins)
            self.initBlock.append(
                '\n'.join(display.initBlock)
                .format(
                    string = self.refInput[display.tag],
                    value = self.refInput[display.tag],
                    **display.config
                    )
                )
            self.bodyBlock.append(
                '\n'.join(display.codeBlock)
                .format(
                    string = self.refInput[display.tag],
                    value = self.refInput[display.tag],
                    **display.config
                    )
                )

            self.includeBlock = self.includeBlock.union(set(display.includeBlock))
            self.makeBlock = self.makeBlock.union(set(display.makeBlock))

        for actuator in self.actuators:
            self.initBlock.append(
                '\n'.join(actuator.initBlock)
                .format(
                    state = self.refInput[actuator.dTopic],
                    **actuator.pins
                    )
                )

            self.bodyBlock.append(
                '\n'.join(actuator.codeBlock)
                .format(
                    state = self.refInput[actuator.dTopic],
                    **actuator.pins
                    )
                )

            self.includeBlock = self.includeBlock.union(set(actuator.includeBlock))
            self.makeBlock = self.makeBlock.union(set(actuator.makeBlock))

        for widget in self.widgets:
            if widget.tag in ''.join(self.refInput.keys()):
                new = dict()
                for key in self.refInput.keys():
                    new[key[:6]] = key
                self.ref2Lab[widget.tag] = self.refInput.get(widget.tag, self.refInput[new[widget.tag]])

        self.talkToLab()

    def getBodyBlock(self):
        return "\n\t".join([i for i in self.bodyBlock if i])

    def getIncludeBlock(self):
        return "\n".join([i for i in list(self.includeBlock) if i])

    def getInitBlock(self):
        return "\n\t".join([i for i in list(self.initBlock) if i])

    def getMakeBlock(self):
        return " ".join([i for i in list(self.makeBlock) if i])

    def talkToLab(self):
        topics = self.ref2Lab.keys()
        args = (('"%s", ' * len(self.ref2Lab)) % tuple(topics)).rstrip(", ")
        self.bodyBlock.append(
            "char* topics[%d] = {%s};" %
            (len(self.ref2Lab), args)
        )
        duplicateFilter = list()
        for topic in topics:
            if self.ref2Lab[topic].startswith("var") and self.ref2Lab[topic] not in duplicateFilter:
                duplicateFilter.append(self.ref2Lab[topic])
                self.bodyBlock.append("char %s_s[16];" % self.ref2Lab[topic])
                self.bodyBlock.append(
                    'snprintf(%s_s, 16, "%%f", %s);' % (self.ref2Lab[topic], self.ref2Lab[topic])
                )
        kwargs = []
        for topic in topics:
            if self.ref2Lab[topic].startswith("thresh"):
                kwargs.append(
                    '%s ? "true":"false"' %
                    self.ref2Lab[topic]
                )
            else:
                kwargs.append("%s_s" % self.ref2Lab[topic])
        kwargs = ", ".join(kwargs)
        self.bodyBlock.append(
            "char* values[%d] = {%s};" %
            (len(self.ref2Lab), kwargs)
        )
        self.bodyBlock.append(
            "publish(%d, topics, values);" %
            len(self.ref2Lab)
        )