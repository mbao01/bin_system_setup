import os  # Required for creating, renaming, and moving files

from .blockAid import (
    getActuators,
    getDisplays,
    getFunctions,
    getModules,
    getSensors,
    getUtilities,
    getWidgets,
)  # Required for extracting list of block custom objects
from .labAid import (
    Suite
)  # Required for smart handling of blocks to generate corresponding C and makefiles
from .pinAid import (
    getBoardPin
)  # Required for translating from laboratory suite to board pin representation
from .fileAid import getBinaryUrl, insertCBlock, insertMakeBlock

def json2C(jsonData, boardId, host, boardsDir,
           htmlDir, cTemplate, esp_open_rtos_dir,
           makefile):
    suite = Suite(
        getActuators(jsonData),
        getDisplays(jsonData),
        getFunctions(jsonData),
        getModules(jsonData),
        getSensors(jsonData),
        getUtilities(jsonData),
        getWidgets(jsonData),
    )
    suite.connectBlocks()
    insertCBlock(
        suite.getIncludeBlock(),
        suite.getInitBlock(),
        suite.getBodyBlock(),
        boardsDir,
        htmlDir,
        cTemplate,
        boardId,
    )
    insertMakeBlock(suite.getMakeBlock().replace('<extension>', esp_open_rtos_dir), boardsDir, htmlDir, makefile, boardId)
    return getBinaryUrl(boardsDir, boardId, host)
