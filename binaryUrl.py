import json  # Required for parsing JSON payload
import os
import sys  # Required for grabbing terminal arguments

from parserKit.json2C import json2C  # For mapping JSON payload to a url pointing to the corresponding generated binary

# Convert Javascript primitives - expected during JSON parsing - to Python built-in objects
true = True
false = False
null = None

# File directories
base_dir = '/home'
rel_path = os.path.join
ctemplate = rel_path(base_dir, 'template.c')
makefile = rel_path(base_dir, 'Makefile')
boards_dir = rel_path('/firmware_service/public/boards/')
html_dir = rel_path(base_dir, 'html')
esp_open_rtos_dir = '/home/esp-open-rtos/'

print(
    json2C(
        json.loads(open(sys.argv[1], 'r').read()),
        json.loads(sys.argv[2]), sys.argv[3],
        boards_dir,
        html_dir, ctemplate, esp_open_rtos_dir,
        makefile
    )
)

'''
### Example Print
print(
    json2C(
        [
  {
    "b_type": "sensor",
    "b_class": "temperature",
    "name": "temperature sensor",
    "tag": "ts-01",
    "n_input": 0,
    "n_output": 2,
    "is_source": true,
    "setting": {
      "type": "ds18b20",
      "pins": {
        "pins0": "io4"
      },
      "unit": "celsius",
      "data": {
        "d_topic": "ts-01-f1",
        "d_size": 1,
        "d_actions": [
          "dd-81"
        ]
      },
      "threshold": {
        "value": 32,
        "t_topic": 'ts-01-t1'
      },
      "config": null
    }
  },
  {
    "b_type": "sensor",
    "b_class": "humidity",
    "name": "humidity sensor",
    "tag": "hs-41",
    "n_input": 0,
    "n_output": 2,
    "is_source": true,
    "setting": {
      "type": "ds18b20",
      "pins": {
        "pins0": "adc0"
      },
      "unit": "percent",
      "data": {
        "d_topic": "hs-41-f1",
        "d_size": 1,
        "d_actions": [
          "ld-91"
        ]
      },
      "threshold": {
        "t_topic": "hs-41-t1",
        "value": 50
      },
      "config": null
    }
  },
  {
    "b_type": "display",
    "b_class": "lcd",
    "name": "liquid crystal display",
    "tag": "ld-91",
    "n_input": 1,
    "n_output": 0,
    "is_source": false,
    "setting": {
      "pins": {
        "pins0": "io3",
        "pins1": "io9"
      },
      "data": null,
      "threshold": null,
      "config": {
         "value": "Invent One"
      }
    }
  },
  {
    "b_type": "display",
    "b_class": "dotmatrix",
    "name": "dot matrix display",
    "tag": "dd-81",
    "n_input": 1,
    "n_output": 0,
    "is_source": false,
    "setting": {
      "pins": {
        "pins0": "io3",
        "pins1": "io6",
        "pins2": "io9"
      },
      "data": null,
      "threshold": null,
      "config": {
         "digits": "8",
         "value": "I",
         "cascade_size": 1
      }
    }
  }
],
        '234',
        'me', boards_dir,
           html_dir, ctemplate, esp_open_rtos_dir,
           makefile
    )
)
'''
