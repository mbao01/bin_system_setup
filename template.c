/*
 * --------------------------------------------------------------------------------
 * This is the main invent one firmware code.
 * Authors - Victor Shoaga <victorshoaga@gmail.com>
 *
 * Thanks to Jeroen Domburg <jeroen@spritesmods.com> for writing the core libs 
 * that the provisioning part of the source code is based on. i.e libesphttpd core.
 * --------------------------------------------------------------------------------
 */
#include <string.h>
#include <stdio.h>

#include <espressif/esp_common.h>
#include <etstimer.h>
#include <espressif/esp_system.h>
#include <espressif/esp_timer.h>
#include <espressif/esp_wifi.h>
#include <espressif/esp_sta.h>
#include <espressif/esp_misc.h>
#include <espressif/osapi.h>

#include <espressif/esp8266/eagle_soc.h>
#include <espressif/esp8266/gpio_register.h>
#include <espressif/esp8266/pin_mux_register.h>

#include <libesphttpd/httpd.h>
#include <libesphttpd/httpdespfs.h>
#include <libesphttpd/cgiwifi.h>
#include <libesphttpd/cgiflash.h>
#include <libesphttpd/auth.h>
#include <libesphttpd/espfs.h>
#include <libesphttpd/captdns.h>
#include <libesphttpd/webpages-espfs.h>
#include <libesphttpd/cgiwebsocket.h>
#include <dhcpserver.h>

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <queue.h>
#include <esp/uart.h>

#include <sysparam.h>

#include <espressif/spi_flash.h>

#include <paho_mqtt_c/MQTTESP8266.h>
#include <paho_mqtt_c/MQTTClient.h>

#include <semphr.h>
#include <stdlib.h>
<includeBlock>
#include "http_client_ota.h"


/* You can use http://test.mosquitto.org/ to test mqtt_client instead
 * of setting up your own MQTT server */
#define MQTT_HOST ("192.168.43.97")
#define MQTT_PORT 1883
#define MQTT_USER NULL
#define MQTT_PASS NULL

#define AP_SSID "Invent One"
#define AP_PSK "invent_one"

#define HARD_RESET_GPIO 0
#define LEDGPIO 2

#define BOARD_ID_INIT "TSLA" //Tesla edition

#define SERVER "192.168.43.73"
#define PORT "3030"

bool isJobTaskRunning = false;

SemaphoreHandle_t wifi_alive;
TaskHandle_t xMQTT_handle, xJOB_handle;
QueueHandle_t publish_queue;

char publish_buff[1024];

sysparam_status_t status;
const int status_base = -6;
const char *status_messages[] = {
    "SYSPARAM_ERR_NOMEM",
    "SYSPARAM_ERR_CORRUPT",
    "SYSPARAM_ERR_IO",
    "SYSPARAM_ERR_FULL",
    "SYSPARAM_ERR_BADVALUE",
    "SYSPARAM_ERR_NOINIT",
    "SYSPARAM_OK",
    "SYSPARAM_NOTFOUND",
    "SYSPARAM_PARSEFAILED",
};

uint32_t base_addr, num_sectors;
static ETSTimer resetBtntimer;


void ioLed(int ena) {
    //gpio_output_set is overkill. ToDo: use better mactos
    if (ena) {
        sdk_gpio_output_set((1<<LEDGPIO), 0, (1<<LEDGPIO), 0);
    } else {
        sdk_gpio_output_set(0, (1<<LEDGPIO), (1<<LEDGPIO), 0);
    }
}

static void resetBtnTimerCb(void *arg) {
	static int resetCnt=0;
	if ((GPIO_REG_READ(GPIO_IN_ADDRESS)&(1<<HARD_RESET_GPIO))==0) {
		resetCnt++;
	} else {
		if (resetCnt>=10) { //5 sec pressed
			printf("Re-initializing sysparam region...\n");
            status = sysparam_create_area(base_addr, num_sectors, true);
            if (status == SYSPARAM_OK) {
                // We need to re-init after wiping out the region we've been
                // using.
                status = sysparam_init(base_addr, 0);
				sdk_wifi_station_disconnect();
				sdk_wifi_set_opmode(SOFTAP_MODE); //reset to SOFTAP mode
				printf("Reset to SOFTAP mode. Restarting system...\n");
				sdk_system_restart();
            }
		}
		resetCnt=0;
	}
}


void ioInit() {
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO2_U, FUNC_GPIO2);
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0);
    
	sdk_gpio_output_set(0, 0, (1<<LEDGPIO), (1<<HARD_RESET_GPIO));
	sdk_os_timer_disarm(&resetBtntimer);
	sdk_os_timer_setfn(&resetBtntimer, resetBtnTimerCb, NULL);
	sdk_os_timer_arm(&resetBtntimer, 500, 1);
}

void print_text_value(char *key, char *value) {
    printf("  '%s' = '%s'\n", key, value);
}

void print_binary_value(char *key, uint8_t *value, size_t len) {
    size_t i;

    printf("  %s:", key);
    for (i = 0; i < len; i++) {
        if (!(i & 0x0f)) {
            printf("\n   ");
        }
        printf(" %02x", value[i]);
    }
    printf("\n");
}

void provisioning_watcher_task(void *pvParameters) {
    
    char *essid, *passwd;
    uint8_t *bin_value;
    size_t len;
    uint8_t *data;
    

    status = sysparam_get_info(&base_addr, &num_sectors);
    if (status == SYSPARAM_OK) {
        printf("[current sysparam region is at 0x%08x (%d sectors)]\n", base_addr, num_sectors);
    } else {
        printf("[NOTE: No current sysparam region (initialization problem during boot?)]\n");
        num_sectors = DEFAULT_SYSPARAM_SECTORS;
        base_addr = sdk_flashchip.chip_size - (5 + num_sectors) * sdk_flashchip.sector_size;
    }
    while (true) {
        status = 0;
        status = sysparam_get_string("wssid", &essid);
        if (status == SYSPARAM_OK) {
            print_text_value("wssid", essid);
        } else if (status == SYSPARAM_PARSEFAILED) {
            status = sysparam_get_data("wssid", &bin_value, &len, NULL);
            if (status == SYSPARAM_OK) {
                print_binary_value("wssid", bin_value, len);
            }
        }
        status = sysparam_get_string("wpasswd", &passwd);
        if (status == SYSPARAM_OK) {
            print_text_value("wpasswd", passwd);
        } else if (status == SYSPARAM_PARSEFAILED) {
            status = sysparam_get_data("wpasswd", &bin_value, &len, NULL);
            if (status == SYSPARAM_OK) {
                print_binary_value("wpasswd", bin_value, len);
            }
        }
        
        if (status != SYSPARAM_OK) {
			//Do something more reasonable here: e.g rgb led keeps varying in multi color blinking
            //printf("! Operation returned status: %d (%s)\n", status, status_messages[status - status_base]);
			//printf("Must resume provisioning mode!!!");
            ioLed(0);

        } else {
			free(essid);
			free(passwd);
			vTaskDelete(NULL);
		}

    }
}

HttpdBuiltInUrl builtInUrls[]={
	{"/", cgiRedirect, "/index.html"},
	{"/connect.cgi", cgiWiFiStoreDetails, NULL},
    {"/connstatus.cgi", cgiWiFiConnStatus, NULL},
	{"*", cgiEspFsHook, NULL},
	{NULL, NULL, NULL}
};

void wifiInit() {
    struct ip_info ap_ip;
    uint8_t sdk_wifi_get_opmode();
    switch(sdk_wifi_get_opmode()) {
        case STATIONAP_MODE:
        case SOFTAP_MODE:
            IP4_ADDR(&ap_ip.ip, 172, 16, 0, 1);
            IP4_ADDR(&ap_ip.gw, 0, 0, 0, 0);
            IP4_ADDR(&ap_ip.netmask, 255, 255, 0, 0);
            sdk_wifi_set_ip_info(1, &ap_ip);

            struct sdk_softap_config ap_config = {
                .ssid = AP_SSID,
                .ssid_hidden = 0,
                .channel = 3,
                .ssid_len = strlen(AP_SSID),
                .authmode = AUTH_WPA_WPA2_PSK,
                .password = AP_PSK,
                .max_connection = 3,
                .beacon_interval = 100,
            };
            sdk_wifi_softap_set_config(&ap_config);

            ip_addr_t first_client_ip;
            IP4_ADDR(&first_client_ip, 172, 16, 0, 2);
			
            dhcpserver_start(&first_client_ip, 4);
            dhcpserver_set_dns(&ap_ip.ip);
            dhcpserver_set_router(&ap_ip.ip);
			
            break;
        case STATION_MODE:
            break;
        default:
            break;
    }
}

int ets_printf(const char *format, ...)
{	
	return 0;
}

void wifi_task(void *pvParameters)
{
    uint8_t status;
	while (true)
    {
        status = sdk_wifi_station_get_connect_status();
        int8_t retries = 30;
        while ((status != STATION_GOT_IP) && (retries > 0))
        {
            printf(".");
            status = sdk_wifi_station_get_connect_status();
            if (status == STATION_WRONG_PASSWORD)
            {
                printf("WiFi: Wrong password\n");
                break;
            }
            else if (status == STATION_NO_AP_FOUND)
            {
                printf("WiFi: AP not found\n");
                break;
            }
            else if (status == STATION_CONNECT_FAIL)
            {
                printf("WiFi: Connection failed\n");
                break;
            }
            vTaskDelay(500 / portTICK_PERIOD_MS);
            --retries;
        }
        if (status == STATION_GOT_IP)
        {
            printf("WiFi: Connected\n");
            vTaskDelay(500 / portTICK_PERIOD_MS);
        }
        while ((status = sdk_wifi_station_get_connect_status()) == STATION_GOT_IP)
        {
            xSemaphoreGive( wifi_alive );
            taskYIELD();
        }
        printf("WiFi: Disconnected\n");
        //force connect to WIFI here 
        sdk_wifi_station_disconnect();
        vTaskDelay(500 / portTICK_PERIOD_MS);
    }
}


static inline void ota_error_handling(OTA_err err) {
    printf("Error:");

    switch(err) {
        case OTA_DNS_LOOKUP_FALLIED:
            printf("DNS lookup has fallied\n");
            break;
        case OTA_SOCKET_ALLOCATION_FALLIED:
            printf("Impossible allocate required socket\n");
            break;
        case OTA_SOCKET_CONNECTION_FALLIED:
            printf("Server unreachable, impossible connect\n");
            break;
        case OTA_SHA_DONT_MATCH:
            printf("Sha256 sum does not fit downloaded sha256\n");
            break;
        case OTA_REQUEST_SEND_FALLIED:
            printf("Impossible send HTTP request\n");
            break;
        case OTA_DOWLOAD_SIZE_NOT_MATCH:
            printf("Dowload size don't match with server declared size\n");
            break;
        case OTA_ONE_SLOT_ONLY:
            printf("rboot has only one slot configured, impossible switch it\n");
            break;
        case OTA_FAIL_SET_NEW_SLOT:
            printf("rboot cannot switch between rom\n");
            break;
        case OTA_IMAGE_VERIFY_FALLIED:
            printf("Dowloaded image binary checsum is fallied\n");
            break;
        case OTA_UPDATE_DONE:
            printf("Ota has completed upgrade process, all ready for system software reset\n");
            break;
        case OTA_HTTP_OK:
            printf("HTTP server has response 200, Ok\n");
            break;
        case OTA_HTTP_NOTFOUND:
            printf("HTTP server has response 404, file not found\n");
            break;
    }
}

static void ota_task(void *PvParameter)
{
    vTaskDelete(xJOB_handle);
    vTaskDelete(xMQTT_handle);
    // Wait until we have joined AP and are assigned an IP *
    while (sdk_wifi_station_get_connect_status() != STATION_GOT_IP)
        vTaskDelay(100 / portTICK_PERIOD_MS);

    while (1) {
        OTA_err err;
        // Remake this task until ota work
        err = ota_update((ota_info *) PvParameter);

        ota_error_handling(err);

        if(err != OTA_UPDATE_DONE) {
            vTaskDelay(1000 / portTICK_PERIOD_MS);
            printf("\n\n\n");
            continue;
        }

        vTaskDelay(1000 / portTICK_PERIOD_MS);
        printf("Delay 1\n");
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        printf("Delay 2\n");
        vTaskDelay(1000 / portTICK_PERIOD_MS);
        printf("Delay 3\n");

        printf("Reset\n");
        sdk_system_restart();
    }
}

static void topic_received(mqtt_message_data_t *md){
    int i;
    mqtt_message_t *message = md->message;
    uint32_t free_heap = xPortGetFreeHeapSize();
    uint32_t free_stack = uxTaskGetStackHighWaterMark(NULL);
    printf(" free heap %u, free stack %u", free_heap, free_stack * 4);
    printf(" Received length : %d\n", (int)message->payloadlen);
    char TOPIC[(int)md->topic->lenstring.len + 1];
    char PAYLOAD[(int)message->payloadlen + 1];
    for( i = 0; i < md->topic->lenstring.len; ++i)
        TOPIC[i] = ((char*)(md->topic->lenstring.data))[i];
    for( i = 0; i < (int)message->payloadlen; ++i)
        PAYLOAD[i] = ((char *)(message->payload))[i];
    TOPIC[(int)md->topic->lenstring.len] = '\0';
    PAYLOAD[(int)message->payloadlen] = '\0';
    printf("%s == %s\n", TOPIC, PAYLOAD);

    if(strcmp(TOPIC, "/board/token/status") == 0){
        printf("STATUS ROUTE \n");
    }else if(strcmp(TOPIC, "/board/token/ota") == 0){
        printf("OTA ROUTE \n");
        printf("Starting OTA!!");
        static char bin_payload[100] = {};
        strcpy(bin_payload, PAYLOAD);
        bin_payload[(int)message->payloadlen] = '\0';
        printf("got = %s\n", bin_payload);
        static ota_info info = {
            .server      = SERVER,
            .port        = PORT,
            .binary_path = bin_payload,
            .sha256_path = NULL,
        };
        
        xTaskCreate(ota_task, "get_task", 2048, &info, 5, NULL); 
    }else{
        printf("MQTT 404 \n");
    }            
}

static void publish(int num_of_topics, char** topics, char** values){
    //char buff[256];
    int i = 0;
    sprintf(publish_buff, "{");
    for(i = 0; i < num_of_topics; i++){
        sprintf(publish_buff + strlen(publish_buff), "\"");
        sprintf(publish_buff + strlen(publish_buff), topics[i]);
        sprintf(publish_buff + strlen(publish_buff), "\"");
        sprintf(publish_buff + strlen(publish_buff), ":");
        if(strcmp("false", values[i]) == 0 || strcmp("true", values[i]) == 0){
            sprintf(publish_buff + strlen(publish_buff), values[i]);
        }else{
            sprintf(publish_buff + strlen(publish_buff), "\"");
            sprintf(publish_buff + strlen(publish_buff), values[i]);
            sprintf(publish_buff + strlen(publish_buff), "\"");
        }
        if(i != num_of_topics - 1){
            sprintf(publish_buff + strlen(publish_buff), ",");
        }
    }
    sprintf(publish_buff + strlen(publish_buff), "}");
    printf("publish_buff - %s\n", publish_buff);
    printf("%d\n", strlen(publish_buff));
    publish_buff[strlen(publish_buff) + 1] = '\0';
    xQueueSend(publish_queue, (void *)publish_buff, 0);
}

static const char *  get_my_id(void)
{
    // Use MAC address for Station as unique ID
    static char my_id[13];
    static bool my_id_done = false;
    int8_t i;
    uint8_t x;
    if (my_id_done)
        return my_id;
    if (!sdk_wifi_get_macaddr(STATION_IF, (uint8_t *)my_id))
        return NULL;
    for (i = 5; i >= 0; --i)
    {
        x = my_id[i] & 0x0F;
        if (x > 9) x += 7;
        my_id[i * 2 + 1] = x + '0';
        x = my_id[i] >> 4;
        if (x > 9) x += 7;
        my_id[i * 2] = x + '0';
    }
    my_id[12] = '\0';
    my_id_done = true;
    return my_id;
}

static void  mqtt_task(void *pvParameters)
{
    int ret = 0;
    struct mqtt_network network;
    mqtt_client_t client   = mqtt_client_default;
    char mqtt_client_id[20];
    uint8_t mqtt_buf[sizeof(publish_buff)];
    uint8_t mqtt_readbuf[100]; 
    mqtt_packet_connect_data_t data = mqtt_packet_connect_data_initializer;

    mqtt_network_new( &network );
    memset(mqtt_client_id, 0, sizeof(mqtt_client_id));
    strcpy(mqtt_client_id, BOARD_ID_INIT);
    strcat(mqtt_client_id, get_my_id());

    printf("CLIENT-ID = %s\n", mqtt_client_id);

    while(1) {
        
    xSemaphoreTake(wifi_alive, portMAX_DELAY);
        printf("%s: started\n\r", __func__);
        printf("%s: (Re)connecting to MQTT server %s ... ",__func__,
               MQTT_HOST);
        ret = mqtt_network_connect(&network, MQTT_HOST, MQTT_PORT);
        
        if( ret ){
            printf("error: %d\n\r", ret);
            taskYIELD();
            continue;
        }

        printf("done\n\r");
        mqtt_client_new(&client, &network, 2000, mqtt_buf, sizeof(publish_buff),
                      mqtt_readbuf, 100);

        data.willFlag       = 0;
        data.MQTTVersion    = 3;
        data.clientID.cstring   = mqtt_client_id;
        data.username.cstring   = MQTT_USER;
        data.password.cstring   = MQTT_PASS;
        data.keepAliveInterval  = 10;
        data.cleansession   = 0;
        printf("Send MQTT connect ... ");
        ret = mqtt_connect(&client, &data);
        if(ret){
            printf("error: %d\n\r", ret);
            mqtt_network_disconnect(&network);
            continue;
        }
        printf("done\r\n");
             
        mqtt_subscribe(&client, "/board/token/#", MQTT_QOS1, topic_received);
        
        xQueueReset(publish_queue);

        while(1){

            //char msg[PUB_MSG_LEN - 1] = "\0";
            while(xQueueReceive(publish_queue, (void *)publish_buff, 0) ==
                  pdTRUE){
                printf("got message to publish\r\n");
                mqtt_message_t message;
                message.payload = publish_buff;
                message.payloadlen = 146;
                message.dup = 0;
                message.qos = MQTT_QOS1;
                message.retained = 0;
                ret = mqtt_publish(&client, "/board/topic", &message);
                if (ret != MQTT_SUCCESS ){
                    printf("error while publishing message: %d\n", ret );
                    taskYIELD();
                    break;
                }
            }

            ret = mqtt_yield(&client, 1000);
            if (ret == MQTT_DISCONNECTED)
                break;
        }
        printf("Connection dropped, request restart\n\r");
        mqtt_network_disconnect(&network);
        taskYIELD();
    }
}

//*************************************************job task runs here******************************************************///
static void job_task(void *pvParameters)
{
    <initBlock>
    while (1) {
        xSemaphoreTake(wifi_alive, portMAX_DELAY);

        //***********************code goes in here*******************//
        <bodyBlock>
        //************************************************************//

        /*
         * Delay(500ms) = 0.5s
         * 
        */
        vTaskDelay(500/portTICK_PERIOD_MS);

    

    }
}
//*************************************************************************************************************************///



void user_init(void) {
    uart_set_baud(0, 115200);
    printf("SDK version: %s, free heap %u\n", sdk_system_get_sdk_version(), xPortGetFreeHeapSize());
    vSemaphoreCreateBinary(wifi_alive);
    publish_queue = xQueueCreate(1, sizeof(publish_buff));
    if(publish_queue == NULL){
        printf("Queue was not created");
    }
    
	//Initialise the necessary to ease provisioning...
	wifiInit();
	captdnsInit();
	espFsInit((void*)(_binary_build_web_espfs_bin_start));
	httpdInit(builtInUrls, 80); 
   
    ioInit();
    
	xTaskCreate(provisioning_watcher_task, "provisioning_watcher", 512, NULL, tskIDLE_PRIORITY + 2, NULL);
    xTaskCreate(mqtt_task, "mqtt_task", 1024, NULL, tskIDLE_PRIORITY + 4, &xMQTT_handle);  //1kb
    xTaskCreate(job_task, "job_task", 1024, NULL, tskIDLE_PRIORITY + 3, &xJOB_handle); //1kb  
	xTaskCreate(wifi_task, "wifi_task", 256, NULL, tskIDLE_PRIORITY + 1, NULL);
}
