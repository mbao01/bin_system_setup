PROGRAM = iofirmware
EXTRA_COMPONENTS = /home/esp-open-rtos/extras/dhcpserver /home/esp-open-rtos/extras/rboot-ota /home/esp-open-rtos/extras/libesphttpd /home/esp-open-rtos/extras/paho_mqtt_c  /home/esp-open-rtos/extras/onewire_temp /home/esp-open-rtos/extras/ds18b20 /home/esp-open-rtos/extras/onewire /home/esp-open-rtos/extras/analog_light /home/esp-open-rtos/extras/digital_write /home/esp-open-rtos/extras/rboot-ota /home/esp-open-rtos/extras/mbedtls /home/esp-open-rtos/extras/http_client_ota <makeBlock>
#Tag for OTA images. 0-27 characters. Change to eg your projects title.
LIBESPHTTPD_OTA_TAGNAME ?= generic
#Tag for OTA images. 0-27 characters. Change to eg your projects title.
LIBESPHTTPD_OTA_TAGNAME ?= generic

LIBESPHTTPD_MAX_CONNECTIONS ?= 8
LIBESPHTTPD_STACKSIZE ?= 2048

PROGRAM_CFLAGS += -DFREERTOS -DLIBESPHTTPD_OTA_TAGNAME="\"$(LIBESPHTTPD_OTA_TAGNAME)\"" -DFLASH_SIZE=$(FLASH_SIZE)
EXTRA_CFLAGS += -DMEMP_NUM_NETCONN=$(LIBESPHTTPD_MAX_CONNECTIONS)

include /home/esp-open-rtos/common.mk

